# Installation

### Prerequisites:

* Ensure Oracle VirtualBox is installed: https://www.virtualbox.org/wiki/Downloads
* Ensure Vagrant is installed and correctly configured: https://www.vagrantup.com/downloads.html
* Ensure Vagrant vb-guest plugin is installed: `vagrant plugin install
  vagrant-vbguest`

### Instructions

```
mkdir ~/git-projects
cd ~/git-projects
git clone https://gitlab.com/sohaibazed/docker-training.git
cd docker-training
vagrant up
```

### Verification

SSH into each vagrant box and verify docker engine is available

```
vagrant ssh master
sudo su 
docker run hello-world
exit
```
